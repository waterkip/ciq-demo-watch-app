# SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesley@opperschaap.net>
#
# SPDX-License-Identifier: BSD-3-Clause

include /etc/garmin-connectiq/Makefile.ciq

MY_PROJECT := BugExposeApp
