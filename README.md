The Buggy App
==

This app started out as a way to display bugs in CIQ but it also can help you
with code examples of things and to test barrels.

The app is pretty simple, it opens a menu where you can hop to different parts
of the system and there you can setup a playground for yourself.

# Dependencies

This codebase relies on the [OPN
barrel](https://waterkip.gitlab.io/opn-monkeyc) being present. You should have
a copy of this barrel in `../barrels/OPN/`.

It also uses docker/docker-compose.yml for development on Linux.

If you use docker, you can tweak this by overriding the monkey.jungle file and
loading the barrel from a different location.

```
# docker-compose.override.yml
    volumes:
      - ../OPN:/home/ciq/barrels/OPN
```
