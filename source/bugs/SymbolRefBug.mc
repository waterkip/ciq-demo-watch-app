// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

import Toybox.Lang;

class SymbolRefBug {

  public var noCollision;
  protected var alsoNoCollision;
  private var collision;

  function initialize(args as Dictionary) {

    if (!args.hasKey(:noCollision)) {
      args.put(:noCollision, 1);
    }

    if (!args.hasKey(:alsoNoCollision)) {
      args.put(:alsoNoCollision, 1);
    }

    // Emits: Unable to detect scope for the symbol reference 'collision'.
    if (!args.hasKey(:collision)) {
      args.put(:collision, 1);
    }

  }
}
