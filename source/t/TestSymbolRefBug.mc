// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

(:test)
module TestSymbolRefBug {
using OPN.Test.More as t;
using Toybox.System as Sys;
import Toybox.Lang;

    (:test)
    function testSymbolRefBug(logger) {
      new SymbolRefBug({});
      return true;
    }
}
