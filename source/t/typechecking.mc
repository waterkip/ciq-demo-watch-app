// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

(:test)
module TestTypechecking {
using OPN.Test.More as t;
using Toybox.System as Sys;
import Toybox.Lang;
import Toybox.Test;

enum FOO {
  boo = "Boo",
  foo,
  bar,
  baz = 500,
  jada,
}

  function yada(z as String, x as String) as String {
    return z + x;
  }

  class Foo {
    function initialize() {
    }

    function foo() as Foo {
      return new Foo();
    }
  }

    (:test)
    function testTypechecker(logger as Test.Logger) as Boolean {
      //var x = yada(1,2); // without would be 3
      //t.diag(x);
      var x = yada("Works ", "for me"); // yay
      t.diag(x);

      var f = new Foo();
      var b = f.foo();
      b.foo();

      t.diag(boo);
      t.diag(foo);
      t.diag(bar);
      t.diag(baz);
      t.diag(jada);
      return true;
    }


}
