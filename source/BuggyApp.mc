// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;


var profiles;
var profileDeletedId;

var showLayers;
var setImmediate;

class TheBuggyApp extends Application.AppBase {

    function initialize() {
        AppBase.initialize();

        profiles = new Profiles();
        profileDeletedId = null;

        showLayers   = false;
        setImmediate = false;
    }

    // onStart() is called on application start up
    function onStart(state as Dictionary?) as Void {
    }

    // onStop() is called when your application is exiting
    function onStop(state as Dictionary?) as Void {
    }

    // Return the initial view of your application here
    function getInitialView() as Array<Views or InputDelegates>? {
        return [ new MainMenu(), new MainMenuDelegate() ] as Array<Views or InputDelegates>;
    }

}

function getApp() as TheBuggyApp {
    return Application.getApp() as TheBuggyApp;
}
