// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using OPN.X11.Menu2 as M2;
using Toybox.System as Sys;

class PullupBannerMenu extends Rez.Menus.PullupBannerMenu {

  function initialize() {
    Rez.Menus.PullupBannerMenu.initialize();
  }
}

class PullupBannerMenuDelegate extends M2.Delegate {

    function initialize() {
      M2.Delegate.initialize(null);
    }

    function onSelect(item) {
      M2.Delegate.onSelect(item);

      pushView(
        new PullupBannerView(itemId),
        new PullupBannerViewDelegate()
      );
    }

    function onBack() {
      popView();
    }


}
