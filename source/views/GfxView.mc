// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using OPN.Math.Coordinates as g;

class GfxView extends Ui.View {

  private var width;
  private var height;

  function initialize() {
    Ui.View.initialize();
    self.width = null;
    self.height = null;
  }

  function onLayout(dc as Gfx.Dc) {
    // You could opt for rounded-280-280 resource
    self.width  = dc.getWidth();
    self.height = dc.getHeight();
  }

  function onShow() {
    System.println("onShow");
  }

  function onUpdate(dc as Gfx.Dc) {
    System.println("onUpdate");
    dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLUE);
    dc.clear();
    setWindowInfo(dc);
    drawLine(dc);
    setupShop(dc);
  }

  function setupShop(dc) {
    drawMinutes(dc);
    drawMinutesBlock(dc);
  }

  function setWindowInfo(dc) {
    dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLUE);
    dc.drawText(
      self.width / 2,
      self.height / 2,
      Gfx.FONT_TINY,
      Lang.format("width: $1$, height: $2$", [ self.width, self.height ]),
      Gfx.TEXT_JUSTIFY_CENTER
    );
    var l = g.getCircleCoords({:size => self.width, :degrees => 315});
    var r = g.getCircleCoords({:size => self.width, :degrees => 45});
    dc.drawText(
      self.width / 2,
      self.height / 4,
      Gfx.FONT_TINY,
      Lang.format("l: $1$\nr: $2$", [ l[0], r[0]]),
      Gfx.TEXT_JUSTIFY_CENTER
    );
  }

  function drawLine(dc as Gfx.Dc) {
    var x_start = 0;
    var y_start = self.height / 2;

    var x_end = self.height;
    var y_end = y_start;

    dc.setPenWidth(1);
    dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLUE);
    dc.drawLine(x_start, y_start, x_end, y_end);
  }

  function drawMinutesBlock(dc as Gfx.Dc) {
    var l = g.getCircleCoords({:size => self.width, :degrees => 315});
    var r = g.getCircleCoords({:size => self.width, :degrees => 45});

    dc.setColor(Graphics.COLOR_YELLOW, Graphics.COLOR_BLUE);
    dc.fillCircle(r[0], r[1], 10);
    dc.fillCircle(l[0], l[1], 10);

    l = g.getCircleCoords({:size => self.width, :degrees => 135});
    r = g.getCircleCoords({:size => self.width, :degrees => 225});
    dc.fillCircle(l[0], l[1], 10);
    dc.fillCircle(r[0], r[1], 10);


    var size = r[0] - l[0];
    var xy;
    dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_BLUE);
    for (var i = 0 ; i < 60; i+=5) {
      xy = g.getBoxCoords({ :canvas => self.width, :size => size, :minutes => i});
      dc.fillCircle(xy[0], xy[1], 5);
    }

  }

  function drawMinutes(dc as Gfx.Dc) {

    dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLUE);
    for (var i = 0 ; i < 60; i+=5) {
      var xy = g.getCircleCoords({:canvas => self.width, :size => self.width -5 , :minute => i});
      dc.fillCircle(xy[0], xy[1], 10);
    }

  }

}

class GfxViewDelegate extends Ui.BehaviorDelegate {

  function initialize() {
    Ui.BehaviorDelegate.initialize();
  }

  function onMenu() {
    System.println("Menu behavior triggered");
    // Setting this to false will cause the onKey() to be triggered
    return true;
  }

  function onSelect() {
    System.println("Select behavior triggered");
    return false;
  }

  function onBack() {
    Ui.popView(DEFAULT_ACTION);
    return true;
  }

  function onNextPage() {
    System.println("Pressed on down");
    return false;
  }

  function onPreviousPage() {
    System.println("Pressed on up");
    return false;
  }

  function onKey(keyEvent) {
    Ui.requestUpdate();
    return true;
  }

}
