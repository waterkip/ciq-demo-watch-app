// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.Timer;
using OPN.X11.Layer as L;
using OPN.X11.View as V;
import Toybox.Lang;

class BannerView extends V.ViewX {

  private var item;
  private var banner;

  function initialize(item) {
    V.ViewX.initialize(null);
    // onLayout seems to be the actual constructor, because there we actually
    // have access to the w/h, dc etc.
    self.banner   = null;
    self.item    = item;

    $.showLayers    = false;
  }

  function onLayout(dc as Gfx.Dc) {
    V.ViewX.onLayout(dc);

    self.banner = new L.Banner({
      :dc => dc,
      :position => self.item
    });

    addLayer(self.banner);
  }

  function onUpdateCanvas(dc as Gfx.Dc) {

    if ($.showLayers != self.banner.isVisible()) {
      self.banner.setVisible($.showLayers);
    }

    dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLUE);
    dc.clear();

    if (!self.banner.isVisible()) {
      return;
    }

    var bdc = self.banner.getDc();
    bdc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_YELLOW);
    bdc.clear();
    var info = self.item.toString();
    var fnt = Gfx.FONT_TINY;
    bdc.drawText(
      bdc.getWidth() / 2,
      bdc.getHeight() /2,
      fnt,
      info,
      Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_CENTER
    );
  }

}

class BannerViewDelegate extends Ui.BehaviorDelegate {

  function initialize() {
    Ui.BehaviorDelegate.initialize();
  }

  function onMenu() {
    return false;
  }

  function onSelect() {
    $.showLayers = !$.showLayers;
    return false;
  }

  function onBack() {
    Ui.popView(DEFAULT_ACTION);
    return true;
  }

  // Up button
  function onPreviousPage() {
    return false;
  }

  // Down button
  function onNextPage() {
    return false;
  }

  function onKey(keyEvent) {
    Ui.requestUpdate();
    return true;
  }
}
