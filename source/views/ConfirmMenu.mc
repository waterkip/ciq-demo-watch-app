// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using OPN.X11.Menu2 as M2;
import Toybox.Lang;

class ConfirmMenu extends Ui.Menu2 {

  function initialize() {
    Menu2.initialize({:title => "Yes/no/maybe"});
    if (Toybox.WatchUi has :TextPicker) {
      addMenuItem(:textPicker, "Add item");
    }
  }

  function refresh() {
    var ids = $.profiles.getIds() as Array<String>;
    for (var i =0; i< ids.size(); i++) {
      var shim = $.profiles.getShim(ids[i]) as Array<String>;
      Sys.println(shim);
      upsertProfileShim(shim);
    }
    if ($.profileDeletedId != null) {
      M2.deleteItem(self, $.profileDeletedId);
    }
  }

  function onShow() {
    Sys.println("onShow() was called");
    refresh();
  }

  private function upsertProfileShim(shim) as Void {
    var id = shim.get(:id);
    var name = shim.get(:name);
    var itemId = findItemById(id);
    if (itemId > -1) {
      M2.setItemLabel(self, id, name);
      return;
    }
    addMenuItem(id, name);
    return;
  }

  private function addMenuItem(id, label) {
    addItem( new Ui.MenuItem(label, "", id, {}));
  }

}

class ConfirmMenuDelegate extends M2.Delegate {
  function initialize() {
    M2.Delegate.initialize(null);
  }

  function onSelect(item) {
    M2.Delegate.onSelect(item);

    if (itemId instanceof Lang.String) {
      if (Toybox.WatchUi has :TextPicker) {
        pushView(
            new Ui.Confirmation(Lang.format("Delete $1$?", [ itemLabel ])),
            new ConfirmOrDenyDelegate(itemId)
        );
      }
      else {
        diag("Cannot select item to delete, because cannot add item");
      }
    }
    else if (itemId == :textPicker) {
      pushView(
        new Ui.TextPicker("foobarbaz"),
        new MyTextPickerDelegate()
      );
    }

  }
}

class ConfirmOrDenyDelegate extends Ui.ConfirmationDelegate {

  var id;
  function initialize(id) {
    ConfirmationDelegate.initialize();
    self.id = id;
  }

  function onResponse(response) as Lang.Boolean {
      if (response == Ui.CONFIRM_YES) {
        $.profiles.delete(self.id);
        //
        // BUG BUG BUG BUG
        // The view doesn't get reloaded correctly even tho it should
        // None of the solutions work:
        //Ui.pushView(new ConfirmMenu(), new ConfirmMenuDelegate(), DEFAULT_ACTION);
        //Ui.switchToView(new ConfirmMenu(), new ConfirmMenuDelegate(), DEFAULT_ACTION);
        // Go back to the old view.. I don't want that
        //Ui.popView(DEFAULT_ACTION);
        //
        // The issue might be related to the fact that we don't know WHICH item
        // got deleted and we can't remove the item ID's. But this exposes a
        // problem with the Menu2 objec that doesn't have a function to ask
        // which menuItems it currently holds. Which requires us to keep a var
        // alive for each menu that has the selected item id so we can delete
        // it afterwards?
        //
        $.profileDeletedId = self.id;
        return true;
      }
      return false;
  }
}

class MyTextPickerDelegate extends Ui.TextPickerDelegate {

  public function initialize() {
    Ui.TextPickerDelegate.initialize();
  }

  public function onTextEntered(name, changed) as Boolean {
    // We don't allow no text, just stay where we are
    if (name.length() == 0) {
      return false;
    }

    $.profiles.add(name);
    return true;
  }

  public function onCancel() as Boolean {
    return true;
  }
}
