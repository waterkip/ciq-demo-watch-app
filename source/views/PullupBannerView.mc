// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.Timer;
using OPN.X11.Layer as L;
import Toybox.Lang;

class PullupBannerView extends Ui.View {

  private var _width;
  private var _height;
  private var layer;

  private var item;

  function initialize(item) {
    Ui.View.initialize();
    // onLayout seems to be the actual constructor, because there we actually
    // have access to the w/h, dc etc.
    self._width  = null;
    self._height = null;
    self.layer   = null;
    self.item    = item;
  }

  //
  // onLayout() is a special thing, while it has access to a Dc it will not paint
  // the full picture if you have layers.
  // Or as FlowState says:
  // Great question. I will just make another wild guess (since those always
  // turn out so well >_>) that onLayout() is called too early to do any layout
  // rendering, although drawing stuff on the (main) dc works at that point.
  // Suggested reading: https://forums.garmin.com/developer/connect-iq/f/discussion/301701/struggling-a-bit-with-views-and-how-stuff-gets-on-the-screen
  //

  function onLayout(dc as Gfx.Dc) {
    $.setImmediate = false;

    self._width  = dc.getWidth();
    self._height = dc.getHeight();

    if (self.item == :slideDown) {
      onLayoutSlideDown(dc);
    }
    else if (self.item == :slideUp) {
      onLayoutSlideUp(dc);
    }
    else if (self.item == :slideRight) {
      onLayoutSlideRight(dc);
    }
    else if (self.item == :slideLeft) {
      onLayoutSlideLeft(dc);
    }

    $.showLayers = false;
    addLayer(self.layer);
  }

  function onLayoutSlideDown(dc as Gfx.Dc) {
    self.layer   = new L.AnimatedBanner({
      :locX       => 0,
      :locY       => 0,
      :width      => self._width,
      :height     => self._height / 2,
      // We extend a normal layer
      // These are the default options of the PullupBanner
      // :speed      => 50,
      // :steps      => 7,
      // :slide      => Ui.SLIDE_UP,
    });
  }

  function onLayoutSlideUp(dc as Gfx.Dc) {
    self.layer   = new L.AnimatedBanner({
      :locX       => 0,
      :locY       => 140,
      :width      => self._width,
      :height     => self._height / 2,
      :slide      => Ui.SLIDE_UP,
      :dc         => dc,
    });
  }

  function onLayoutSlideRight(dc as Gfx.Dc) {
    self.layer   = new L.AnimatedBanner({
      :locX       => 0,
      :locY       => 0,
      :width      => self._width * 2/3,
      :height     => self._height,
      :slide      => Ui.SLIDE_RIGHT,
      //:dc         => dc,
    });
  }

  function onLayoutSlideLeft(dc as Gfx.Dc) {
    self.layer   = new L.AnimatedBanner({
      :locX       => self._width * 1/3 +10,
      :locY       => 0,
      :width      => self._width * 2/3,
      :height     => self._height,
      :slide      => Ui.SLIDE_LEFT,
      :dc         => dc,
    });
  }

  //
  // onUpdate needs to be implemented because we don't have a layout set, so we
  // don't have any drawable objects. The WatchUI.View.onUpdate() just
  // overrides the dc.setColor() and ANY objects you may or may not have set in
  // onLayout leaving you with a black screen and rather puzzled of why things
  // aren't working the way you expect them to work. It only took me several
  // days to figure this out :/
  // You'll need View.onUpdate() most likely when you have a layout, because
  // than this method will actually pain the layout you have set and you can
  // see all the nice things you have made.
  // onUpdate need to paint the whole layout, even if you think it shouldn't:
  // accept it as a fact and your life is sooo much easier.
  //
  function onUpdate(dc as Gfx.Dc) {
    dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLUE);
    dc.clear();


    var fnt = Gfx.FONT_TINY;
    var fntH = dc.getFontHeight(fnt);
    var diff = 5;

    var info = $.setImmediate ? "Direct" : "Animated transition";

    if (self.item == :slideDown) {
      dc.drawText(
        dc.getWidth() / 2,
        dc.getHeight() /2 + diff,
        fnt,
        info,
        Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_CENTER
      );
      dc.drawText(
        dc.getWidth() / 2,
        dc.getHeight() /2 + diff + fntH,
        fnt,
        "Menu comes down and up",
        Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_CENTER
      );
    }
    else if (self.item == :slideUp) {
      dc.drawText(
        dc.getWidth() / 2,
        dc.getHeight() /2 - diff - fntH,
        fnt,
        info,
        Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_CENTER
      );
      dc.drawText(
        dc.getWidth() / 2,
        dc.getHeight() / 2 - diff,
        fnt,
        "Menu comes up and down",
        Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_CENTER
      );
    }
    else {
      dc.drawText(
        dc.getWidth() / 2,
        dc.getHeight() / 2,
        fnt,
        "Menu left and right",
        Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_CENTER
      );
    }

    showAnimatedLayers();
  }

  function showAnimatedLayers() as Void {

    if ($.setImmediate) {
      self.layer.setVisibleNow($.showLayers);
    }
    else {
      self.layer.setVisible($.showLayers);
    }
    if (!self.layer.isVisible()) {
      return;
    }

    if (self.item == :slideDown) {
      showSlideDown();
    }
    else if (self.item == :slideUp) {
      showSlideUp();
    }
    else if (self.item == :slideRight) {
      showSlideRight();
    }
    else if (self.item == :slideLeft) {
      showSlideLeft();
    }
  }

  function showSlideDown() {
    var lcd = self.layer.getDc();
    lcd.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_YELLOW);
    lcd.clear();
    lcd.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
    lcd.setPenWidth(4);
    lcd.drawLine(0, lcd.getHeight(), lcd.getWidth(), lcd.getHeight());
    lcd.drawText(
      lcd.getWidth() / 2,
      lcd.getHeight() - 30,
      Gfx.FONT_TINY,
      "Going up",
      Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_CENTER
    );
  }

  function showSlideUp() {
    var lcd = self.layer.getDc();
    lcd.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_YELLOW);
    lcd.clear();
    lcd.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
    lcd.setPenWidth(4);
    lcd.drawLine(0, 0, lcd.getWidth(), 0);

    lcd.drawText(
      lcd.getWidth() / 2, 30,
      Gfx.FONT_TINY,
      "Going down",
      Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_CENTER
    );
  }

  function showSlideRight() {
    var lcd = self.layer.getDc();
    lcd.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_YELLOW);
    lcd.clear();
    lcd.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
    lcd.setPenWidth(4);
    lcd.drawLine(lcd.getWidth(), 0, lcd.getWidth(), lcd.getHeight());

    var x = lcd.getTextDimensions("Going right", Gfx.FONT_TINY) as Array<Number>;

    lcd.drawText(
      lcd.getWidth() - (x[0]/2),
      lcd.getHeight() / 2,
      Gfx.FONT_TINY,
      "Going right",
      Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_CENTER
    );
  }

  function showSlideLeft() {
    var lcd = self.layer.getDc();
    lcd.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_YELLOW);
    lcd.clear();
    lcd.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
    lcd.setPenWidth(4);
    lcd.drawLine(0, 0, 0, lcd.getHeight());

    var x = lcd.getTextDimensions("Going Left", Gfx.FONT_TINY) as Array<Number>;

    lcd.drawText(
      0 + x[0] / 2,
      lcd.getHeight() / 2,
      Gfx.FONT_TINY,
      "Going left",
      Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_CENTER
    );
  }

}

class PullupBannerViewDelegate extends Ui.BehaviorDelegate {

  function initialize() {
    Ui.BehaviorDelegate.initialize();
  }

  function onMenu() {
    return false;
  }

  function onSelect() {
    $.showLayers = !$.showLayers;
    return false;
  }

  function onBack() {
    $.setImmediate = true;
    $.showLayers = false;
    Ui.requestUpdate();
    Ui.popView(DEFAULT_ACTION);
    return true;
  }

  // Up button
  function onPreviousPage() {
    $.setImmediate = !$.setImmediate;
    return false;
  }

  // Down button
  function onNextPage() {
    return false;
  }

  function onKey(keyEvent) {
    Ui.requestUpdate();
    return true;
  }
}
