// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using OPN.X11.Menu2 as M2;
using Toybox.System as Sys;

class MainMenu extends Rez.Menus.MainMenu {

  function initialize() {
    Rez.Menus.MainMenu.initialize();
  }

  (:debug)
  function onShow() {
    Sys.println("We are in the main menu");
  }

}

class MainMenuDelegate extends M2.Delegate {

    function initialize() {
      M2.Delegate.initialize(null);
    }

    function onSelect(item) {
      M2.Delegate.onSelect(item);

      switch(itemId) {
        case :exitApp :
          popView();
          break;
        case :bugConfirmation :
          pushView(new ConfirmMenu(), new ConfirmMenuDelegate());
          break;
        case :legal :
          pushView(new LegalView(), new LegalViewDelegate());
          break;
        case :gfxView :
          pushView(new GfxView(), new GfxViewDelegate());
          break;
        case :banners :
          pushView(new BannerMenu(), new BannerMenuDelegate());
          break;
        case :animatedBanner :
          pushView(new PullupBannerMenu(), new PullupBannerMenuDelegate());
          break;
      }
    }

    function onBack() {
      diag("End of application, quit the simulator");
    }


}
