// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using Toybox.Graphics as Gfx;
using Toybox.WatchUi as Ui;
import Toybox.Lang;

class LegalView extends Ui.View {

  private var license;

  function initialize() {
    Ui.View.initialize();
    license = "";
    addText("This is a testing app and not intended for the general public. ");
    addText("If you found this app in the appstore and installed it, ");
    addText("it is best to remove it.");
    addText("\n");
    addText("This app is covered by a BSD license.");
  }

  function addText(text as Lang.String) {
    license += text;
  }

  function onUpdate(dc) {
    dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
    dc.clear();

    var t = new Ui.TextArea({
        :text  => license,
        :color => Graphics.COLOR_WHITE,
        :font  => Gfx.FONT_XTINY,
        :locX  => Ui.LAYOUT_HALIGN_CENTER,
        :locY  => Ui.LAYOUT_VALIGN_CENTER,
        :width => dc.getWidth() - 100,
        :height => dc.getHeight() - 100,
        });

    t.draw(dc);
  }
}

class LegalViewDelegate extends Ui.BehaviorDelegate {

  function initialize() {
    Ui.BehaviorDelegate.initialize();
  }

  function onBack() {
    System.println("This should break as we don't import System");
    Ui.popView(DEFAULT_ACTION);
    return true;
  }
}

