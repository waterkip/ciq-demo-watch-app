// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

using Toybox.Application.Storage as st;
using Toybox.System as Sys;
import Toybox.Lang;

class Profiles {

  var profiles;
  function initialize() {
    self.profiles = st.getValue("xx");
    if (self.profiles == null) {
      self.profiles = {
        "foo" => [ "foo" ],
        "bar" => [ "bar" ],
        "baz" => [ "baz" ],
      };
    }
    st.setValue("xx", self.profiles);
  }

  function getIds() as Array<String> {
    var array = [] as Array<String>;
    array = self.profiles.keys();
    return array;
  }

  function getShim(id) {
    return { :id => id, :name => (self.profiles.get(id) as Array<String>)[0] };
  }


  function add(id) {
    self.profiles.put(id, [ id ]);
    st.setValue("xx", self.profiles);
  }

  function delete(id) {
    if (self.profiles.hasKey(id)) {
      self.profiles.remove(id);
    }
  }

}
